# VSCode 上からファイル選択時、アクティブファイルが上書き動作対応方法

## 原因

- [Preview mode](https://code.visualstudio.com/docs/getstarted/userinterface#_preview-mode)で表示されるため。
- Preview mode とは、表示確認するためだけの表示方法
- VSCode のデフォルト設定は、Preview mode が有効
- Preview mode で開かれている時は、タブ見出しが斜体表示

## Preview mode を使用しない方法

- エクスプローラからファイル選択時ダブルクリック
- VSCode 基本設定の enablePreview のチェックを外す
